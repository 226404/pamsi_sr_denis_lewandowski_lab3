#pragma once
#include "exceptions.h"
#include <iostream>

using namespace std;

template <typename typ>
class StosInkrement {
	typ* stos;
	int rozmiar;
	int n;  // Na ktorym miejscu jest top w stosie
public:
	StosInkrement(int x) {
		stos = new typ[x];
		rozmiar = x;
		n = -1;
	}
	~StosInkrement() {
		if (!czyPusty())
			delete[] stos;
	}
	bool czyPusty();
	virtual void Powieksz();
	void Dodaj(typ wartosc);
	typ Usun();
	void UsunWszystko();
	void Wyswietl();
};

template <typename typ>
bool StosInkrement<typ>::czyPusty()
{
	if (n < 0) return true;
	else return false;
}

template <typename typ>

void StosInkrement<typ>::Powieksz()
{
	rozmiar = rozmiar + 50;
	typ* temp = new typ[rozmiar];
	for (int i = 0; i < n; i++) {
		temp[i] = stos[i];
	}
	delete[] stos;
	stos = temp;
}

template <typename typ>
void StosInkrement<typ>::Dodaj(typ wartosc)
{
	n++;
	if (rozmiar == n) Powieksz();
	stos[n] = wartosc;
}

template <typename typ>
typ StosInkrement<typ>::Usun()
{
	if (czyPusty()) {
		throw EmptyStack();
	}
	return stos[n--];
}

template<typename typ>
void StosInkrement<typ>::UsunWszystko()
{
	if (czyPusty()) {
		throw EmptyStack();
	}
	n = -1;
	delete[] stos;
}

template <typename typ>
void StosInkrement<typ>::Wyswietl()
{
	for (int i = 0; i <= n; i++)
		cout << stos[i] << "\t";
	cout << endl;
}
