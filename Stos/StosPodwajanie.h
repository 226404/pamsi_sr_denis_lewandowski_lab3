#pragma once
#include "stdafx.h"
#include "StosPodwajanie.h"
#include "exceptions.h"
#include <iostream>

using namespace std;

template <typename typ>
class StosPodwajanie {
	typ* stos;
	int rozmiar;
	int n;  // Na ktorym miejscu jest top w stosie
public:
	StosPodwajanie(int x) {
		stos = new typ[x];
		rozmiar = x;
		n = -1;
	}
	~StosPodwajanie() {
		if (n<0)
			delete[] stos;
	}
	void Powieksz();
	bool czyPusty();
	void Dodaj(typ wartosc);
	typ Usun();
	void UsunWszystko();
	void Wyswietl();
};

template <typename typ>
bool StosPodwajanie<typ>::czyPusty()
{
	if (n < 0) return true;
	else return false;
}

template <typename typ>
void StosPodwajanie<typ>::Powieksz()
{
	rozmiar *= 2;
	typ* temp = new typ[rozmiar];
	for (int i = 0; i < n; i++) {
		temp[i] = stos[i];
	}
	delete[] stos;
	stos = temp;
}

template <typename typ>
void StosPodwajanie<typ>::Dodaj(typ wartosc)
{
	n++;
	if (rozmiar == n) Powieksz();
	stos[n] = wartosc;
}

template <typename typ>
typ StosPodwajanie<typ>::Usun()
{
	if (czyPusty()) {
		throw EmptyStack();
	}
	return stos[n--];
}

template<typename typ>
void StosPodwajanie<typ>::UsunWszystko()
{
	if (czyPusty()) {
		throw EmptyStack();
	}
	n = -1;
	delete[] stos;
}

template <typename typ>
void StosPodwajanie<typ>::Wyswietl()
{
	for (int i = 0; i <= n; i++)
		cout << stos[i] << "\t";
	cout << endl;
}