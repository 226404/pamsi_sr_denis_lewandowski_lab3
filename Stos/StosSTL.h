#pragma once
#include "stdafx.h"
#include <iostream>
#include <stack>
#include "exceptions.h"

template <typename typ>
class StosSTL
{
	std::stack <typ> stos;
	
public:
	bool czyPusty();
	void Dodaj(typ wartosc);
	typ Usun();
	void UsunWszystko();
	void Wyswietl();
};

template<typename typ>
bool StosSTL<typ>::czyPusty()
{
	if (stos.empty()) return true;
	return false;
}

template<typename typ>
void StosSTL<typ>::Dodaj(typ wartosc)
{
	stos.push(wartosc);
}

template<typename typ>
typ StosSTL<typ>::Usun()
{
	if(!czyPusty())	stos.pop();
	throw EmptyStack();
}

template<typename typ>
void StosSTL<typ>::UsunWszystko()
{
	while (!czyPusty())
		stos.pop();
}

template<typename typ>
void StosSTL<typ>::Wyswietl()
{
	if (czyPusty()) throw EmptyStack();
	for (std::stack <typ> tmp = stos; !tmp.empty(); tmp.pop())
		std::cout << tmp.top() << "\t ";
	std::cout << endl;
}

