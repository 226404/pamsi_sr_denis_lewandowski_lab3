#include "stdafx.h"
#include "StosInkrementacyjny.h"
#include "StosPodwajanie.h"
#include "StosSTL.h"
#include "StosLista.h"
#include "exceptions.h"
#include <iostream>
#include <exception>
#include <cstdio>
#include <ctime>

using namespace std;

int main()
{
	StosInkrement <int> stos(2);
	try {
		stos.Dodaj(5);
		stos.Dodaj(99);
		stos.Wyswietl();
		stos.Usun();
		stos.Wyswietl();
		stos.Dodaj(11);
		stos.Wyswietl();
		stos.Dodaj(99);
		stos.Dodaj(22);
		stos.Wyswietl();
		stos.UsunWszystko();
		stos.Usun();
	}

	catch (...) {
		cout << "Pusty Stos!" << endl;
	}

	try {
		StosPodwajanie <int> stos2(3);
		stos2.Dodaj(5);
		stos2.Dodaj(100);
		stos2.Dodaj(40);
		stos2.Wyswietl();
	}

	catch (...) {
		cout << "Pusty Stos!" << endl;
	}


	StosSTL <int> stos3;
	try {
		stos3.Dodaj(5);
		stos3.Wyswietl();
		stos3.Usun();
		stos3.Usun();
	}

	catch (...) {
		cout << "Pusty Stos!" << endl;
	}


	// ****************************** MIERZENIE CZASU *****************************************//
	StosInkrement <int> stosTest(1);
	int ileDanychWejsciowych = 10000;
	srand(time(NULL));
	
	double roznica;
	cout.setf(ios::fixed); //notacja zwykla
	cout.precision(4);     //liczba miejsc po przecinku, dokladnosc naszego wyniku	
	clock_t start, koniec; //inicjacja zmiennych zegarowych	
	start = clock();       //zapisanie czasu startu mierzenia

	for (int i = 0; i < ileDanychWejsciowych; i++) {
		stosTest.Dodaj(rand());
	}

	koniec = clock();										//zapisanie konca mierzenia	
	roznica = (koniec - start) / (double)CLOCKS_PER_SEC;   //obliczenie roznicy, czyli czasu wykonania	
	cout << "Czas wykonania: " << roznica << endl;




	getchar();
	getchar();
	return 0;
}


