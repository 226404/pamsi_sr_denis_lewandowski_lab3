#pragma once
#include <iostream>

using namespace std;

struct element {
	element *nastepny;
	int napis;
	element();  // Konstruktor
};
element::element() {
	nastepny = NULL;
}

class StosLista {
private:
	element *pierwszy;			    // Wskaznik na pierwszy element listy

public:
	StosLista() { setPierwszy(NULL); }  //Konstruktor
	element* getPierwszy() { return pierwszy; }
	void setPierwszy(element *pierwszy) { this->pierwszy = pierwszy; }
	void Wyswietl();
	void Usun();
	void Wyczysc();
	void Dodaj(int napis);
};


void StosLista::Wyswietl() {
	element *temp = getPierwszy();
	while (temp) {
		cout << temp->napis << "\t";
		temp = temp->nastepny;
	}
	cout << endl;
}
void StosLista::Usun() {
	element *temp = getPierwszy()->nastepny;
	delete getPierwszy();
	setPierwszy(temp);
}
void StosLista::Wyczysc() {
	element *temp = pierwszy;

	while (getPierwszy()) {
		temp = getPierwszy();
		setPierwszy(getPierwszy()->nastepny);
		delete temp;
	}
}

void StosLista::Dodaj(int napis) {
	element *nowy = new element;
	nowy->napis = napis;

	if (getPierwszy() == NULL) {
		setPierwszy(nowy);
	}
	else {
		element *temp = getPierwszy();
		setPierwszy(nowy);
		getPierwszy()->nastepny = temp;
	}
}