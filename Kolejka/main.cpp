// Kolejka.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "KolejkaTab.h"
#include "exceptions.h"
#include "KolejkaSTL.h"

int main()
{
	KolejkaTab <int> kolejka(10);

	try {
		kolejka.Dodaj(10);
		kolejka.Dodaj(99);
		kolejka.Wyswietl();
		kolejka.Usun();
		kolejka.Wyswietl();
		kolejka.Usun();
		kolejka.Dodaj(11);
		kolejka.Dodaj(99);
		kolejka.Dodaj(43);
		kolejka.Wyswietl();
		kolejka.UsunWszystko();
		kolejka.Usun();
	}

	catch (EmptyQueueException) {
		cout << "Kolejka jest pusta!" << endl;
	}
	catch (FullQueueException) {
		cout << "Przekroczenie wymiarow tablicy!" << endl;
	}

	getchar();
    return 0;
}

