#pragma once
#include <iostream>
#include "stdafx.h"
#include "exceptions.h"
#include <queue>

using namespace std;

template <typename typ>
class KolejkaSTL
{
	queue <typ> kolejka;
public:
	void Dodaj(typ wartosc);
	void Usun();
	void Wyswietl();
	void UsunWszystko();
};

template<typename typ>
inline void KolejkaSTL<typ>::Dodaj(typ wartosc)
{
	kolejka.push(wartosc);
}

template<typename typ>
inline void KolejkaSTL<typ>::Usun()
{
	if (kolejka.empty()) throw EmptyQueueException();
	else kolejka.pop();
}

template<typename typ>
inline void KolejkaSTL<typ>::Wyswietl()
{
		for (queue <typ> tmp = kolejka; !tmp.empty(); tmp.pop()) {
			cout << tmp.back() << "\t";
		}
		cout << endl;
}

template<typename typ>
inline void KolejkaSTL<typ>::UsunWszystko()
{
	if (kolejka.empty()) throw EmptyQueueException();
	else
		while (!kolejka.empty()) {
			Usun();
		}
}