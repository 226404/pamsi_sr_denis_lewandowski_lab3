#pragma once
#include <iostream>
#include "stdafx.h"
#include "exceptions.h"

using namespace std;

template <typename typ> 
class KolejkaTab
{
	typ *kolejka;
	int N;		// rozmiar tablicy
	int f;		// indeks pierwszego elementu
	int r;		// indeks nastepny do ostatniego
public:
	KolejkaTab(int x);
	~KolejkaTab();
	void Dodaj(typ wartosc);
	void Usun();
	void Wyswietl();
	void UsunWszystko();
	bool czyPusta();
	int Rozmiar();
};

template<typename typ>
inline KolejkaTab<typ>::KolejkaTab(int x)
{
	kolejka = new typ[x];
	N = x;
	f = 0;
	r = 0;
}

template<typename typ>
inline KolejkaTab<typ>::~KolejkaTab()
{
	delete[] kolejka;
}

template<typename typ>
inline void KolejkaTab<typ>::Dodaj(typ wartosc)
{
	if (Rozmiar() == N - 1)
		throw FullQueueException();
	else {
		kolejka[r] = wartosc;
		r = (r + 1) % N;
	}
}

template<typename typ>
inline void KolejkaTab<typ>::Usun()
{
	if (czyPusta()) throw EmptyQueueException();
	else {
		f = (f + 1) % N;  
	}
}

template<typename typ>
inline void KolejkaTab<typ>::Wyswietl()
{
	for (int i = f; i < r; i++) {
		cout << kolejka[i] << "\t";
	}
	cout << endl;
}

template<typename typ>
inline void KolejkaTab<typ>::UsunWszystko()
{
	delete[] kolejka;
	f = 0;
	r = 0;
}

template<typename typ>
inline bool KolejkaTab<typ>::czyPusta()
{
	if (f == r) return true;
	return false;
}

template<typename typ>
inline int KolejkaTab<typ>::Rozmiar()
{
	return (N - f + r) % N;
}
